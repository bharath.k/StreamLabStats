<?php
namespace app\helpers;
use app\models\Streams;
use app\models\FollowedStreams;

class twitchHelper {
	const TWITCH_ID_DOMIAN = 'https://id.twitch.tv/';
	const TWITCH_API_DOMIAN = 'https://api.twitch.tv/helix/';

	private $_clientId;
	private $_clientSecret;
	private $_accessToken;
	private $_refreshToken;

	public function __construct( $clientId, $clientSecret, $accessToken = '' ) {
		$this->_clientId = $clientId;
		$this->_clientSecret = $clientSecret;
		$this->_accessToken = $accessToken;
	}

	public function getLoginUrl( $redirectUri ) {
		$endpoint = self::TWITCH_ID_DOMIAN . 'oauth2/authorize';
		$_SESSION['twitch_state'] = md5( microtime() . mt_rand() );
		$params = array(
			'client_id' => $this->_clientId,
			'redirect_uri' => $redirectUri,
			'response_type' => 'code',
			'scope' => 'user:read:email',
			'state' => $_SESSION['twitch_state']
		);
		return $endpoint . '?' . http_build_query( $params );
	}

	public function LoginWithTwitch( $code, $redirectUri ) {
		$accessToken = $this->getTwitchAccessToken( $code, $redirectUri );
		$status = $accessToken['status'];
		$message = $accessToken['message'];
		$data = [];
		if ( $status == 'ok' ) { 
			$this->_accessToken = $accessToken['api_data']['access_token'];
			$this->_refreshToken = $accessToken['api_data']['refresh_token'];
			$userInfo = $this->makeCurlCall(self::TWITCH_API_DOMIAN . 'users', 'GET', $this->getAuthorizationHeaders());

			$status = $userInfo['status'];
			$message = $userInfo['message'];
		
			if ($userInfo['status'] == "ok" && isset( $userInfo['api_data']['data'][0] ) ) {
				$data['twitch_user_info'] = $_SESSION['twitch_user_info'] = $userInfo['api_data']['data'][0];
				$data['twitch_user_info']['access_token'] = $_SESSION['twitch_user_info']['access_token'] = $this->_accessToken;
				$data['twitch_user_info']['refresh_token'] = $_SESSION['twitch_user_info']['refresh_token'] = $this->_refreshToken;
			}
		}
		return array(
			'status' => $status,
			'message' => $message,
			'data' => $data
		);
	}

	public function updateStreams($twitchApi, $all_streams, $pagination, $count) {    
		$current_streams = [];
        if ($count == 100) {
            $current_streams = $twitchApi->getStreams($pagination);
            $count = count($current_streams['streams']);
            $all_streams = array_merge($all_streams, $current_streams['streams']);
            $twitchApi->updateStreams($twitchApi, $all_streams, $current_streams['pagination'], $count);
        }
        return ['all_stream' => $all_streams, 'pagination' => $pagination];
    }    

	public function getStreams($pagination = "") {
		$url = self::TWITCH_API_DOMIAN . 'streams?first=100' . (!empty($pagination) ? '&after='.$pagination : "");
		$streams = $this->makeCurlCall($url, 'GET', $this->getAuthorizationHeaders());
		$status = $streams['status'];
		$message = $streams['message'];
		$data = [];
		if ($streams['status'] == "ok") {
			$data['streams'] = $streams['api_data']['data'];
			$data['pagination'] = $streams['api_data']['pagination']['cursor'];
		}
		return $data;
	}

	public function updateFollowedStreams($twitchApi, $all_streams, $pagination, $count) {    
		$current_streams = [];
        if ($count < 1001) {
            $current_streams = $twitchApi->getFollowedStreams($pagination);
            $count = count($all_streams);
            $all_streams = array_merge($all_streams, $current_streams['streams']);
            $twitchApi->updateFollowedStreams($twitchApi, $all_streams, $current_streams['pagination'], $count, $pagination);
        }
        return ['all_stream' => $all_streams, 'pagination' => $pagination];
    }    

	public function getFollowedStreams($pagination = "") {
		$url = self::TWITCH_API_DOMIAN . 'streams/followed?first=100' . (!empty($pagination) ? '&after='.$pagination : ""). '&user_id='.$_SESSION['user_id'].'&scope=user:read:follows';
		$streams = $this->makeCurlCall($url, 'GET', $this->getAuthorizationHeaders());
		$status = $streams['status'];
		$message = $streams['message'];
		$data = [];
		if ($streams['status'] == "ok") {
			$data['streams'] = $streams['api_data']['data'];
			$data['pagination'] = $streams['api_data']['pagination']['cursor'];
		}
		return $data;
	}

	public function getChannels() {
		$channelsInfo = $this->makeCurlCall(self::TWITCH_API_DOMIAN . 'channels', 'GET', $this->getAuthorizationHeaders());
		$status = $channelsInfo['status'];
		$message = $channelsInfo['message'];
		$data = [];
		if ($channelsInfo['status'] == "ok") {
			$data['channels_info'] = $channelsInfo['api_data']['data'];
		}
		return $data;
	}

	public function getTags($tag_ids = []) {
		$url = self::TWITCH_API_DOMIAN . 'tags/streams';
		if (!empty($tag_ids)) {
			foreach($tag_ids as $tag_id) {
				$url = $url . ((strpos($url, '?') == false) ? '?tag_id='.$tag_id :'&tag_id='.$tag_id);
			}
		}
		$tags = $this->makeCurlCall($url, 'GET', $this->getAuthorizationHeaders());
		$status = $tags['status'];
		$message = $tags['message'];
		$data = [];
		if ($tags['status'] == "ok") {
			$data['tags'] = $tags['api_data']['data'];
		}
		return $data;
	}

	public function getGames() {
		$topGames = $this->makeCurlCall(self::TWITCH_API_DOMIAN . 'games?first=100', 'GET', $this->getAuthorizationHeaders());
		return $topGames;
	}

	public function getTopGames() {
		$topGames = $this->makeCurlCall(self::TWITCH_API_DOMIAN . 'games/top?first=100', 'GET', $this->getAuthorizationHeaders());
		$status = $topGames['status'];
		$message = $topGames['message'];
		$data = [];
		if ($topGames['status'] == "ok") {
			$top_games = $topGames['api_data']['data'];
			foreach($top_games as $top_game) {
				$streams = Streams::find()->where(['game_id' => $top_game['id']])->count();
				$top_game['stream_count'] = $streams;
				$data['top_games'][] = $top_game;
			}
		}
		return $data;
	}

	public function getAuthorizationHeaders() {
		return array(
			'Client-ID: ' . $this->_clientId,
			'Authorization: Bearer ' . $this->_accessToken
		);
	}

	public function getTwitchAccessToken( $code, $redirectUri ) 
	{
		$access_token = $this->makeCurlCall(self::TWITCH_ID_DOMIAN."oauth2/token?client_id=".$this->_clientId."&client_secret=".$this->_clientSecret."&code=".$code."&grant_type=authorization_code&redirect_uri=".$redirectUri, 'POST');
		return $access_token;
	}

	public function makeCurlCall(string $url, string $type = 'GET', array $headers = [], array $post_fields = [], string $user_agent = '', string $referrer = '', bool $follow = true, bool $use_ssl = false, int $con_timeout = 10, int $timeout = 40)
	{
		$crl = curl_init($url);
		curl_setopt($crl, CURLOPT_CUSTOMREQUEST, $type);
		if (!empty($user_agent)) {
			curl_setopt($crl, CURLOPT_USERAGENT, $user_agent);
		}
		if (!empty($referrer)) {
			curl_setopt($crl, CURLOPT_REFERER, $referrer);
		}
		if ($type == 'POST') {
			curl_setopt($crl, CURLOPT_POST, true);
			if (!empty($post_fields)) {
				curl_setopt($crl, CURLOPT_POSTFIELDS, $post_fields);
			}
		}
		if (!empty($headers)) {
			curl_setopt($crl, CURLOPT_HTTPHEADER, $headers);
		}
		curl_setopt($crl, CURLOPT_FOLLOWLOCATION, $follow);
		curl_setopt($crl, CURLOPT_CONNECTTIMEOUT, $con_timeout);
		// curl_setopt($crl, CURLOPT_NOBODY, false);
		curl_setopt($crl, CURLOPT_HTTP_VERSION, $timeout);
		curl_setopt($crl, CURLOPT_SSL_VERIFYHOST, $use_ssl);
		curl_setopt($crl, CURLOPT_SSL_VERIFYPEER, $use_ssl);
		// curl_setopt($crl, CURLOPT_ENCODING, ''); 
		curl_setopt($crl, CURLOPT_RETURNTRANSFER, true);
		$call_response = curl_exec($crl);
		$http_response_code = curl_getinfo($crl, CURLINFO_HTTP_CODE);
		curl_close($crl);
		if ($http_response_code == 200) {
			return array(
				'status' => 'ok', // if status then there was an error
				'message' => isset( $call_response['message'] ) ? $call_response['message'] : '', // if message return it
				'api_data' => json_decode($call_response, true) // api response data
			);
		} else {
			return array(
				'http_response_code' => $http_response_code, 
				'status' => 'fail',
				'message' => isset( $call_response['message'] ) ? $call_response['message'] : ''
			);//Call failed
		}
	}
}