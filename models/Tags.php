<?php 
namespace app\models;
use yii;

class Tags extends \yii\db\ActiveRecord
{
    public $tag_id;
    public $localization_names;
    public $localization_descriptions;

    public static function tableName()
    {
        return '{{tags}}';
    }

    public function rules() {
        return [
            [['tag_id', 'localization_names'], 'required'],
            [['email', 'localization_names', 'localization_descriptions'], 'string'],
        ];
     }

    public function attributeLabels()
    {
        return [
            'tag_id' => 'Twitch Id',
            'localization_names' => 'Localization names',
            'localization_descriptions' => 'Localization Descriptions'
        ];
    }

    public static function primaryKey()
    {
        return ['tag_id'];
    }

    public function save($runValidation = true, $attributeNames = null, $isExists = false)
    {
        if (!$isExists) {
            return $this->insert($runValidation, $this->attributes());
        } else {
            return $this->update($runValidation, $attributeNames) !== false;
        }
    }
}