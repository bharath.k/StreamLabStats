<?php 
namespace app\models;
use yii\db\ActiveRecord;
use app\models\Tags;

class FollowedStreams extends ActiveRecord
{
    public $id;
    public $user_id;
    public $user_login;
    public $user_name;
    public $game_id;
    public $title;
    public $game_name;
    public $thumbnail_url;
    public $viewer_count;
    public $tag_ids;
    public $started_at;

    public static function tableName()
    {
        return 'followed_streams';
    }

    public function rules()
    {
        return [
            [['id', 'user_id', 'user_login', 'user_name', 'game_id', 'title', 'game_name', 'thumbnail_url', 'viewer_count'], 'required'],
            [['id', 'viewer_count'], 'integer'],
            [['user_id', 'user_login', 'user_name', 'game_id', 'title', 'game_name', 'thumbnail_url', 'tag_ids'], 'string'],
        ];
    }

    public static function primaryKey()
    {
        return ['id'];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'user_login' => 'User Login',
            'user_name' => 'User Name',
            'game_id' => 'Game ID',
            'title' => 'Title',
            'game_name' => 'Game Name',
            'thumbnail_url' => 'Thunbnail URL',
            'viewer_count' => 'Viewers Count',
            'tag_ids' => 'Tag IDs'
        ];
    }

    public static function getFollowedStreams($user_id) {
        $all_streams = FollowedStreams::find()->limit(1000)->all();
        return $all_streams;
    }

    public function save($runValidation = true, $attributeNames = null, $isExists = false)
    {
        if (!$isExists) {
            return $this->insert($runValidation, $this->attributes());
        } else {
            return $this->update($runValidation, $attributeNames) !== false;
        }
    }

    public static function sharedTags() {
        $all_streams = FollowedStreams::find()->limit(1000)->select(['tag_ids'])->all();
        $all_tags = [];
        foreach ($all_streams as $stream) {
            $all_tags = array_merge($all_tags, explode(',',$stream->getAttribute('tag_ids')));
        }
        $tag_names = [];
        $tags = Tags::find()->where(['id' => $all_tags])->all();
        foreach ($tags as $tag) {
            if (!empty($tag) && !empty($tag->getAttribute('id')) && !empty($tag->getAttribute('localization_names'))) {
                $tag_names[$tag->getAttribute('id')] = $tag->getAttribute('localization_names');
            }
        }
        return $tag_names;
    }
}