<?php 
namespace app\models;
use yii;

class User extends \yii\db\ActiveRecord
{
    public $twitch_id;
    public $email;
    public $username;
    public $access_token;
    public $refresh_token;
    public $created_on;

    public static function tableName()
    {
        return '{{users}}';
    }

    public function rules() {
        return [
            [['twitch_id', 'email', 'username', 'access_token', 'refresh_token'], 'required'],
            ['email', 'email'],
            [['twitch_id','created_on'], 'integer'],
            [['email', 'username', 'access_token', 'refresh_token'], 'string'],
        ];
     }

    public function attributeLabels()
    {
        return [
            'twitch_id' => 'Twitch Id',
            'email' => 'email address',
            'username' => 'username',
            'access_token' => 'Access Token',
            'refresh_token' => 'Refresh Token',
            'created_on' => 'created on'
        ];
    }

    public static function primaryKey()
    {
        return ['twitch_id'];
    }

    public function save($runValidation = true, $attributeNames = null, $isExists = false)
    {
        if (!$isExists) {
            return $this->insert($runValidation, $this->attributes());
        } else {
            return $this->update($runValidation, $attributeNames) !== false;
        }
    }
}