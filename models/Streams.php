<?php 
namespace app\models;
use yii\db\ActiveRecord;

class Streams extends ActiveRecord
{
    public $id;
    public $user_id;
    public $user_login;
    public $user_name;
    public $game_id;
    public $title;
    public $game_name;
    public $thumbnail_url;
    public $viewer_count;
    public $tag_ids;
    public $started_at;

    public static function tableName()
    {
        return 'streams';
    }

    public function rules()
    {
        return [
            [['id', 'user_id', 'user_login', 'user_name', 'game_id', 'title', 'game_name', 'thumbnail_url', 'viewer_count'], 'required'],
            [['id', 'viewer_count'], 'integer'],
            [['user_id', 'user_login', 'user_name', 'game_id', 'title', 'game_name', 'thumbnail_url', 'tag_ids'], 'string'],
        ];
    }

    public static function primaryKey()
    {
        return ['id'];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'user_login' => 'User Login',
            'user_name' => 'User Name',
            'game_id' => 'Game ID',
            'title' => 'Title',
            'game_name' => 'Game Name',
            'thumbnail_url' => 'Thunbnail URL',
            'viewer_count' => 'Viewers Count',
            'tag_ids' => 'Tag IDs'
        ];
    }

    public function save($runValidation = true, $attributeNames = null, $isExists = false)
    {
        if (!$isExists) {
            return $this->insert($runValidation, $this->attributes());
        } else {
            return $this->update($runValidation, $attributeNames) !== false;
        }
    }

    function getClosest($search, $arr) {
        $closest = null;
        foreach ($arr as $item) {
           if ($closest === null || abs($search - $closest) > abs($item - $search)) {
              $closest = $item;
           }
        }
        return $closest;
     }

    public static function getFollowedStreams($user_id) {
        $lowestStream = FollowedStreams::find()->orderBy(['viewer_count' => SORT_ASC])->select('viewer_count')->one();
        $topStreams = FollowedStreams::find()->orderBy(['viewer_count' => SORT_DESC])->limit(1000)->select('viewer_count')->all();
        $lowestStreamViewers = $lowestStream->getAttribute('viewer_count');
        $all_viewers = [];
        foreach ($topStreams as $stream) {
            $all_viewers[] = $stream->getAttribute('viewer_count');
        }
        $diff = Self::getClosest($lowestStrea, $all_viewers);
        if (count($all_viewers) < 1000) {
            return "All streams are in range of 1 - 1000";
        }else if(empty($diff)) {
            $diff = $all_viewer[count($all_viewers) - 1] - $lowestStream;
        }
        return $diff;
    }

    public static function getStreamViewerMedian() {
        $all_viewers = [];
        $median = 0;
		$all_streams = Streams::find()->orderBy(['viewer_count' => SORT_ASC])->select('viewer_count')->all();
        foreach ($all_streams as $stream) {
            $all_viewers[] = $stream->getAttribute('viewer_count');
        }
        $count = sizeof($all_viewers);
        $index = floor($count/2);
        if ($count & 1) {
            $median = $all_viewers[$index];
        } else {
            $median = ($all_viewers[$index-1] + $all_viewers[$index]) / 2;
        }
        return $median;
	}
}