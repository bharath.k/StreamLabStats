<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
* {box-sizing: border-box}
.mySlides1 {display: none}
img {vertical-align: middle;}

/* Slideshow container */
.slideshow-container {
  max-width: 1000px;
  position: relative;
  margin: auto;
}

/* Next & previous buttons */
.prev, .next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  padding: 16px;
  margin-top: -22px;
  color: white;
  font-weight: bold;
  font-size: 18px;
  transition: 0.6s ease;
  border-radius: 0 3px 3px 0;
  user-select: none;
}

/* Position the "next button" to the right */
.next {
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a grey background color */
.prev:hover, .next:hover {
  background-color: #f1f1f1;
  color: black;
}
</style>
</head>
<body>

<h2 style="text-align:center">StreamLab Stats</h2>

<p>Top Games</p>
<div class="slideshow-container">
    <?php 
        if(!empty($top_games)) {
            foreach($top_games as $key => $game) {
                echo '<div class="mySlides1">
                            <img src='.str_replace('-{width}x{height}', '', $game['box_art_url']).' alt='.$game['name'].' width="1000" height="500">
                        <div class="text">'.$game['name'].'</div>
                        <div class="text"> <b>stream count -</b>'.$game['stream_count'].'</div>
                </div>';
            }
        }
    ?>

  <a class="prev" onclick="plusSlides(-1, 0)">&#10094;</a>
  <a class="next" onclick="plusSlides(1, 0)">&#10095;</a>
</div>
<div> <b>Median number of viewers for all streams - <?php echo $median;?></b> </div>
<div> <b>viewers does the lowest viewer count stream that the logged in user is following need to gain in order to make it into the top 1000- <?php echo $diff;?></b> </div>
<script>
var slideIndex = [1,1];
var slideId = ["mySlides1"]
showSlides(1, 0);
showSlides(1, 1);

function plusSlides(n, no) {
  showSlides(slideIndex[no] += n, no);
}

function showSlides(n, no) {
  var i;
  var x = document.getElementsByClassName(slideId[no]);
    if (n > x.length) {slideIndex[no] = 1}    
    if (n < 1) {slideIndex[no] = x.length}
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";  
    }
    x[slideIndex[no]-1].style.display = "block";  
}
</script>

</body>
</html> 
