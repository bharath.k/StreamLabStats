<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>stream labs</h1>

        <p class="lead">All-in-one live streaming software</p>

        <p><a class="btn btn-lg btn-success" href="https://streamlabs.com/">Explore us</a></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Fast and backed up to the cloud</h2>
                <p>One simple, feature-packed streaming software with everything you need to stream to Twitch, YouTube and Facebook in seconds.</p>
            </div>
            <div class="col-lg-4">
                <h2>Personalize streams with pro overlays</h2>

                <p>Free stream overlays and hundreds more with Prime. Look professional when you stream on Twitch, YouTube and Facebook!</p>

                <p><a class="btn btn-default" href="https://streamlabs.com/themes"> Browse themes and overlays</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Multistream — stream to multiple platforms</h2>

                <p>Stream to YouTube, Twitch, Facebook, and more at once to expand your reach. Just toggle it on and go-live.</p>

                <p><a class="btn btn-default" href="https://streamlabs.com/">Try multistreaming</a></p>
            </div>
        </div>

    </div>
</div>
