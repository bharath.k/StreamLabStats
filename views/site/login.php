<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap4\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;
use app\helpers\twitchHelper;
// $config = require __DIR__ . '/../config/web.php';
$is_logged = !empty($_SESSION['twitch_user_info']['display_name']);

$this->title = !$is_logged ? 'Login' : 'Welcome';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>

        <div class="form-group">
            <div class="offset-lg-1 col-lg-11">
                <?php
                    $twitchApi = new twitchHelper(Yii::$app->params['clientId'], Yii::$app->params['clientSecret']);
                    $twitchLoginUrl = $twitchApi->getLoginUrl("http://localhost:8888/index.php?r=site/login");
                ?>
                <?php if (!$is_logged ) { ?>
                    <a href=<?php echo $twitchLoginUrl;?> class="a-twitch">
                    <div class="btn btn-primary">
                        Login with Twitch
                    </div>
                </a>
                <?php } else {?>
                    <div>
                        Welcome <?php echo $_SESSION['twitch_user_info']['display_name']; ?>
                    </div>
                <?php } ?>
            </div>
        </div>
</div>
