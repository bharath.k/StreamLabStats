<?php
namespace app\commands;
use Yii;
use yii\console\Controller;
use yii\console\ExitCode;

use app\helpers\twitchHelper;
use app\models\Streams;
use app\models\Tags;

use fedemotta\cronjob\models\CronJob;
/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 */
class StreamController extends Controller
{
    /**
     * This command echoes what you have entered as the message.
     * @param string $message the message to be echoed.
     * @return int Exit code
     */
    public function actionIndex($message = 'Cron Start')
    {
        $twitchApi = new twitchHelper(Yii::$app->params['clientId'], Yii::$app->params['clientSecret'], Yii::$app->params['accessToken']);
        $all_streams = [];
        $streams = $twitchApi->getStreams();
        $all_streams = $streams['streams'];
        if (!empty($all_streams)) {
            $stream_data = $twitchApi->updateStreams($twitchApi, $all_streams, $streams['pagination'], count($all_streams));
            $all_streams = $stream_data['all_stream'];
            $remaining_streams = $twitchApi->getStreams($stream_data['pagination']);
            $all_streams = array_merge($all_streams, $remaining_streams['streams']);
        }
        // var_dump(count($all_streams));exit;
        $count = 0;
        $tags_ids = [];
        if (!empty($all_streams)) {
            foreach ($all_streams as $stream) {
                $exisitng_record = false;
                $curr_stream = Streams::find()->where(['id' => $stream['id']])->one();
                if (empty($curr_stream)) {
                    $curr_stream = new Streams;
                } else {
                    $exisitng_record = true;
                }
                $curr_stream->setAttribute('id',$stream['id']);
                $curr_stream->setAttribute('user_id', $stream['user_id']);
                $curr_stream->setAttribute('user_login', $stream['user_login']);
                $curr_stream->setAttribute('user_name', $stream['user_name']);
                $curr_stream->setAttribute('game_id', $stream['game_id']);
                $curr_stream->setAttribute('title', preg_replace('/[\x00-\x1F\x7F-\xFF]/', '', $stream['title']));
                $curr_stream->setAttribute('game_name', $stream['game_name']);
                $curr_stream->setAttribute('thumbnail_url', $stream['thumbnail_url']);
                $curr_stream->setAttribute('viewer_count', $stream['viewer_count']);
                $curr_stream->setAttribute('tag_ids', implode(',',$stream['tag_ids']));
                $curr_stream->setAttribute('started_at', $stream['started_at']);
                $tags_ids = array_merge($tags_ids, $stream['tag_ids']);
                if ($curr_stream->save(false, null, $exisitng_record)) {
                    $count = $count + 1;
                }
            }
            // update tags
            if (!empty($tags_ids)) {
                $chunks = array_chunk($tags_ids,100);
                foreach($chunks as $chunk) {
                    $tags = $twitchApi->getTags($chunk)['tags'];
                    if (!empty($tags)) {
                        foreach($tags as $tag) {
                            $exisitng_tag_record = false;
                            $curr_tag = Tags::find()->where(['id' => $tag['tag_id']])->one();
                            if (empty($curr_tag)) {
                                $curr_tag = new Tags;
                            } else {
                                $exisitng_tag_record = true;
                            }
                            $curr_tag->setAttribute('id',$tag['tag_id']);
                            $curr_tag->setAttribute('localization_names', $tag['localization_names']['en-us']);
                            $curr_tag->setAttribute('localization_descriptions', $tag['localization_descriptions']['en-us']);
                            $curr_tag->save(false, null, $exisitng_tag_record);
                        }
                    }
                }
            }
        }
        echo "total $count streams got updated";
    }   
}
