<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\helpers\twitchHelper;
use app\models\User;
use app\models\Streams;
use app\models\FollowedStreams;
use yii\helpers\Url;

class SiteController extends Controller
{

    private $client_id;
    private $client_secret;
    private $access_token;
    private $refresh_token;

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout', 'stats'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        $is_login = false;
        $data = [];
        if ( isset( $_GET['code'] ) && isset( $_GET['state'] ) && $_SESSION['twitch_state'] == $_GET['state']) {
            $twitchApi = new twitchHelper(Yii::$app->params['clientId'], Yii::$app->params['clientSecret']);
            $data = $twitchApi->loginWithTwitch( $_GET['code'], "http://localhost:8888/index.php");
            if (!empty($data['access_token'])) {
                $is_login = true;
            }
        }
        if (!empty($data)) {
            $id = !empty($data['data']['twitch_user_info']['id']) ? $data['data']['twitch_user_info']['id'] : "";
            if (!empty($id)) {
                $exisitng_record = false;
                $user = User::find()->where(['twitch_id' => $id])->one();
                if (empty($user)) {
                    $user = new User;
                } else {
                    $exisitng_record = true;
                }
                $user->setAttribute('twitch_id', $id);
                $user->setAttribute('username', !empty($data['data']['twitch_user_info']['display_name']) ? $data['data']['twitch_user_info']['display_name'] : "");
                $user->setAttribute('email', !empty($data['data']['twitch_user_info']['email']) ? $data['data']['twitch_user_info']['email'] : "");
                $user->setAttribute('access_token', !empty($data['data']['twitch_user_info']['access_token']) ? $data['data']['twitch_user_info']['access_token'] : "");
                $user->setAttribute('refresh_token', !empty($data['data']['twitch_user_info']['refresh_token']) ? $data['data']['twitch_user_info']['refresh_token'] : "");
                $user->setAttribute('created_on', time());

                $_SESSION['user_id'] = $id;
                Yii::$app->params['accessToken'] = $_SESSION['access_token'] = !empty($data['data']['twitch_user_info']['access_token']) ? $data['data']['twitch_user_info']['access_token'] : "";
                $_SESSION['refresh_token'] = !empty($data['data']['twitch_user_info']['refresh_token']) ? $data['data']['twitch_user_info']['refresh_token'] : "";
                $user->save(false, null, $exisitng_record);
            } else {
                $is_login = false;
            }
        }
        if ($is_login) {
            return $this->goHome();
        }
        return $this->render('login');
    }

    public function actionFollowed() {
        $streams = FollowedStreams::find()->limit(100)->all();
        $tag_names = FollowedStreams::sharedTags();
        $data = [];
        foreach ($streams as $stream) {
            $tag_names_sent = $stream_data = [];
            $stream_data['game_name'] = $stream->getAttribute('game_name');
            $stream_data['title'] = $stream->getAttribute('title');
            $stream_data['thumbnail_url'] = $stream->getAttribute('thumbnail_url');
            $tag_ids = explode(',', $stream->getAttribute('tag_ids'));
            foreach ($tag_ids as $tag_id){
                $tag_names_sent[] = $tag_names[$tag_id];
            }
            $stream_data['tag_names'] = $tag_names_sent;
            $data[] = $stream_data;
        }
        return $this->render('followed', ['data' => $data]);
    }

    public function actionStreams()
    {
        $sortType = isset($_GET['sort']) ? ($_GET['sort'] == "desc" ? SORT_DESC : SORT_ASC) : SORT_ASC;
        $streams = Streams::find()->orderBy(['viewer_count' => $sortType])->limit(100)->all();
        $data = [];
        foreach ($streams as $stream) {
            $stream_data = [];
            $stream_data['game_name'] = $stream->getAttribute('game_name');
            $stream_data['title'] = $stream->getAttribute('title');
            $stream_data['thumbnail_url'] = $stream->getAttribute('thumbnail_url');
            $data[] = $stream_data;
        }
        return $this->render('streams', ['data' => $data]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionContact()
    {
        $_SESSION = array();
        session_destroy();
        return $this->render('login');
    }

    public function actionAbout()
    {
        $twitchApi = new twitchHelper(Yii::$app->params['clientId'], Yii::$app->params['clientSecret'], $_SESSION['access_token']);
        $topGames = $twitchApi->getTopGames();
        $median = Streams::getStreamViewerMedian();
        $diff = Streams::getFollowedStreams($_SESSION['user_id']);
        return $this->render('stats', ['top_games' => $topGames['top_games'], 'median' => $median, 'diff' => $diff]);
    }
}
