<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Handles the creation of table `{{%streams}}`.
 */
class m211120_140149_create_streams_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%streams}}', [
            'id'=> Schema::TYPE_INTEGER. ' NOT NULL',
            'user_id'=> Schema::TYPE_STRING,
            'user_login'=> Schema::TYPE_STRING,
            'user_name'=> Schema::TYPE_STRING,
            'game_id'=> Schema::TYPE_STRING,
            'title'=> Schema::TYPE_STRING,
            'thumbnail_url'=> Schema::TYPE_STRING,
            'viewer_count'=> Schema::TYPE_INTEGER,
            'tag_ids' => Schema::TYPE_TEXT,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%streams}}');
    }
}
