<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%users}}`.
 */
class m211120_140843_create_users_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%users}}', [
            'twitch_id' => Schema::TYPE_BIGINT,
            'username' => Schema::TYPE_STRING,
            'email' => Schema::TYPE_STRING,
            'access_token' => Schema::TYPE_STRING,
            'refresh_token' => Schema::TYPE_STRING,
            'created_on' => Schema::TYPE_BIGINT
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%users}}');
    }
}
